package com.atlassian.bitbucket.linky.rest.cloud

import org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric
import java.lang.IllegalArgumentException

fun uniqueRepositoryId() = RepositoryId(randomAlphanumeric(5, 10), randomAlphanumeric(5, 10))

fun readResourceFile(fileName: String) =
    object {}.javaClass.getResource(fileName)?.readText()
        ?: throw IllegalArgumentException("No resource file found '$fileName'")
