package com.atlassian.bitbucket.linky

import assertk.assertThat
import assertk.assertions.hasToString
import assertk.assertions.isFalse
import assertk.assertions.isTrue
import com.atlassian.bitbucket.linky.discovery.RemoteUrl
import com.atlassian.bitbucket.linky.rest.server.BitbucketServer
import com.atlassian.bitbucket.linky.selection.LinesSelection
import io.mockk.every
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import java.net.URI
import java.net.URLEncoder

internal class ServerLinkyTest : AbstractLinkyTest() {

    private lateinit var linky: ServerLinky

    @BeforeEach
    override fun setUp() {
        super.setUp()

        val remoteUrl = RemoteUrl(UriScheme.HTTP, "example.com", 80, "/")

        val hosting = BitbucketServer(URI.create("http://example.com:1234/path/"))
        val serverRepo = BitbucketRepository.Server(
            repository,
            remoteUrl,
            hosting,
            "someProject",
            "someSlug"
        )
        linky = ServerLinky(serverRepo)
    }

    @Test
    fun `test getCommitViewUri with lines selected`() {
        val linkyFile = linkyFile()
        every { lineBlamer.blameLine(linkyFile, 239) } returns Pair("some/File", 405)

        val commitUri = linky.getCommitViewUri("sha", linkyFile, 239)

        assertThat(commitUri).hasToString("http://example.com:1234/path/projects/someProject/repos/someSlug/commits/sha#some/File")
    }

    @Test
    fun `test getCommitViewUri with SHA reference`() {
        val commitUri = linky.getCommitViewUri("abcdef", linkyFile(), 239)

        assertThat(commitUri).hasToString("http://example.com:1234/path/projects/someProject/repos/someSlug/commits/abcdef")
    }

    @Test
    fun `test getSourceViewUri`() {
        val sourceUri = linky.getSourceViewUri()

        assertThat(sourceUri).hasToString("http://example.com:1234/path/projects/someProject/repos/someSlug/browse")
    }

    @Test
    fun `test getSourceViewUri for file`() {
        val sourceUri = linky.getSourceViewUri(linkyFile(), listOf())

        assertThat(sourceUri).hasToString("http://example.com:1234/path/projects/someProject/repos/someSlug/browse/dir/file?at=someRevision")
    }

    @ParameterizedTest
    @ValueSource(strings = ["md", "mkd", "mkdn", "mdown", "markdown"])
    fun `test getSourceViewUri for markdown file`(extension: String) {
        val filename = "file.$extension"
        val linkyFile = linkyFile(filename = filename)

        val sourceUri = linky.getSourceViewUri(linkyFile, listOf())

        assertThat(sourceUri).hasToString("http://example.com:1234/path/projects/someProject/repos/someSlug/browse/dir/$filename?at=someRevision")
    }

    @Test
    fun `test getSourceViewUri for file with lines selected`() {
        val sourceUri = linky.getSourceViewUri(linkyFile(), listOf(LinesSelection(1, 7), LinesSelection(239, 405)))

        assertThat(sourceUri).hasToString("http://example.com:1234/path/projects/someProject/repos/someSlug/browse/dir/file?at=someRevision#1-7,239-405")
    }

    @ParameterizedTest
    @ValueSource(strings = ["md", "mkd", "mkdn", "mdown", "markdown"])
    fun `test getSourceViewUri for markdown file with lines selected`(extension: String) {
        val filename = "file.$extension"
        val linkyFile = linkyFile(filename = filename)

        val sourceUri = linky.getSourceViewUri(linkyFile, listOf(LinesSelection(1, 7), LinesSelection(239, 405)))

        assertThat(sourceUri).hasToString("http://example.com:1234/path/projects/someProject/repos/someSlug/browse/dir/$filename?at=someRevision&useDefaultHandler=true#1-7,239-405")
    }

    @Test
    fun `test getSourceViewUri for path with spaces`() {
        val sourceUri = linky.getSourceViewUri(linkyFile("dir name", "file name"), listOf())

        assertThat(sourceUri).hasToString("http://example.com:1234/path/projects/someProject/repos/someSlug/browse/dir%20name/file%20name?at=someRevision")
    }

    @Test
    fun `test getPullRequestUri when branch absent`() {
        every { repository.currentBranchName } returns null

        val maybePullRequestUri = linky.getPullRequestUri()

        assertThat(maybePullRequestUri.isPresent).isFalse()
    }

    @Test
    fun `test getPullRequestUri when branch present`() {
        every { repository.currentBranchName } returns "someBranch"

        val maybePullRequestUri = linky.getPullRequestUri()

        assertThat(maybePullRequestUri.isPresent).isTrue()
        assertThat(maybePullRequestUri.get())
            .hasToString("http://example.com:1234/path/projects/someProject/repos/someSlug/pull-requests?create&sourceBranch=someBranch")
    }

    @Test
    fun `test getPullRequestUri branch name escaped`() {
        val branchName = "branch#5><|.hello!&])({}@.@$&+_-`'\""
        every { repository.currentBranchName } returns branchName
        val encodedBranch = URLEncoder.encode(branchName, Charsets.UTF_8.name())

        val maybePullRequestUri = linky.getPullRequestUri()

        assertThat(maybePullRequestUri.isPresent).isTrue()
        assertThat(maybePullRequestUri.get())
            .hasToString("http://example.com:1234/path/projects/someProject/repos/someSlug/pull-requests?create&sourceBranch=$encodedBranch")
    }
}
