package com.atlassian.bitbucket.linky.actions.snippet

import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.openapi.util.text.StringUtil
import com.intellij.ui.components.JBTextField
import java.awt.event.KeyAdapter
import java.awt.event.KeyEvent
import javax.swing.BoxLayout
import javax.swing.JCheckBox
import javax.swing.JPanel

class CreateSnippetDialog(project: Project) : DialogWrapper(project, true) {
    private var workspaceModified = false

    private val titleTextField = JBTextField().apply {
        columns = 30
        emptyText.text = "Snippet title"
        setResizable(false)
    }
    private val privateCheckbox = JCheckBox("Private")
    private val titleAccessPanel = JPanel().apply {
        layout = BoxLayout(this, BoxLayout.X_AXIS)
        add(titleTextField)
        add(privateCheckbox)
    }

    private val workspaceTextField = JBTextField().apply {
        emptyText.text = "Workspace (optional)"
        addKeyListener(object : KeyAdapter() {
            override fun keyReleased(e: KeyEvent?) {
                workspaceModified = true
            }
        })
    }
    private val workspacePanel = JPanel().apply {
        layout = BoxLayout(this, BoxLayout.X_AXIS)
        add(workspaceTextField)
    }

    private val mainPanel = JPanel().apply {
        layout = BoxLayout(this, BoxLayout.Y_AXIS)
        add(titleAccessPanel)
        add(workspacePanel)
    }

    init {
        privateCheckbox.isSelected = true

        title = "Create Snippet"
        setResizable(false)

        super.init()
    }

    override fun createCenterPanel() = mainPanel

    override fun getPreferredFocusedComponent() = titleTextField

    fun getSnippetTitle(): String? = StringUtil.nullize(titleTextField.text)

    fun isPrivate() = privateCheckbox.isSelected

    fun workspace(): String? = workspaceTextField.text
}
