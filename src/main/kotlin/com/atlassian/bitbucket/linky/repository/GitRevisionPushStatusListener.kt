package com.atlassian.bitbucket.linky.repository

import com.intellij.openapi.components.service
import git4idea.commands.Git
import git4idea.commands.GitCommand
import git4idea.commands.GitLineHandler
import git4idea.repo.GitRepository
import git4idea.repo.GitRepositoryChangeListener

class GitRevisionPushStatusListener : GitRepositoryChangeListener {
    override fun repositoryChanged(repository: GitRepository) {
        val project = repository.project
        val lineHandler = GitLineHandler(project, repository.root, GitCommand.LOG).apply {
            addParameters("--branches", "--not", "--remotes", "--pretty=format:%H")
        }
        val result = Git.getInstance().runCommand(lineHandler)
        if (result.success()) {
            project.service<RevisionPushStatusService>()
                .registerOutgoingRevisions(repository.root, result.output.toSet())
        }
    }
}
