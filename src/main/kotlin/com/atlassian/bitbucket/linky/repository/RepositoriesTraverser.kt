package com.atlassian.bitbucket.linky.repository

import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.extensions.ExtensionPointName
import com.intellij.openapi.project.Project

interface RepositoriesTraverser {
    fun traverseRepositories(project: Project): Collection<Repository>

    companion object {
        val EP_NAME =
            ExtensionPointName.create<RepositoriesTraverser>("com.atlassian.bitbucket.linky.repositoriesTraverser")
    }
}

fun Project.traverseRepositories(): Collection<Repository> =
    RepositoriesTraverser.EP_NAME.extensions
        .map { it.traverseRepositories(this) }
        .flatten()
