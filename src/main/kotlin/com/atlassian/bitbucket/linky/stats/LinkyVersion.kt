package com.atlassian.bitbucket.linky.stats

object LinkyVersion {
    val version: String = javaClass
        .getResourceAsStream("/META-INF/plugin.xml")
        ?.bufferedReader()
        ?.lines()
        ?.map { it.trim() }
        ?.filter { it.startsWith("<version>") && it.endsWith("</version>") }
        ?.findFirst()
        ?.map { it.substring(9, it.length - 10) }
        ?.orElse("unknown")
        ?: "failed"
}
