package com.atlassian.bitbucket.linky.configuration

import com.atlassian.bitbucket.linky.LINKY_ISSUES_URL
import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.configuration.UpdateChannel.EAP
import com.atlassian.bitbucket.linky.configuration.UpdateChannel.NIGHTLY
import com.atlassian.bitbucket.linky.configuration.UpdateChannel.STABLE
import com.intellij.ide.BrowserUtil
import com.intellij.openapi.options.BoundConfigurable
import com.intellij.openapi.ui.DialogPanel
import com.intellij.ui.EnumComboBoxModel
import com.intellij.ui.SimpleListCellRenderer
import com.intellij.ui.layout.panel
import javax.swing.JList

class LinkyConfigurable : BoundConfigurable(message("preferences.display.name")) {

    override fun createPanel(): DialogPanel =
        panel {
            titledRow(message("preferences.updates.title")) {
                row(message("preferences.updates.channel.label")) {
                    comboBox(
                        model = EnumComboBoxModel(UpdateChannel::class.java),
                        getter = UpdateChannelSelector::getUpdateChannel,
                        setter = UpdateChannelSelector::setUpdateChannel,
                        renderer = object : SimpleListCellRenderer<UpdateChannel>() {
                            override fun customize(
                                list: JList<out UpdateChannel>,
                                value: UpdateChannel?,
                                index: Int,
                                selected: Boolean,
                                hasFocus: Boolean
                            ) {
                                text = when (value) {
                                    STABLE -> message("preferences.updates.channel.stable")
                                    EAP -> message("preferences.updates.channel.eap")
                                    NIGHTLY -> message("preferences.updates.channel.nightly")
                                    else -> ""
                                }
                            }
                        }
                    )
                }
            }
            titledRow(message("preferences.feedback.title")) {
                noteRow(
                    message(
                        "preferences.feedback.text",
                        "<a href=''>${message("preferences.feedback.link.text")}</a>"
                    )
                ) {
                    BrowserUtil.browse(LINKY_ISSUES_URL)
                }
            }
        }
}
