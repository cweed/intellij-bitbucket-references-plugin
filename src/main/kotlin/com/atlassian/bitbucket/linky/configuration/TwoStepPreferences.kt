package com.atlassian.bitbucket.linky.configuration

import com.atlassian.bitbucket.linky.preferences.Preferences

class TwoStepPreferences(private val delegate: Preferences) : Preferences {
    private val intermediateAdd = mutableMapOf<String, String>()
    private val intermediateDelete = mutableSetOf<String>()

    override fun getProperty(key: String, defaultValue: String?): String? =
        intermediateAdd[key] ?: delegate.getProperty(key, defaultValue)

    override fun setProperty(key: String, value: String) {
        // remove the key from properties to remove
        intermediateDelete.remove(key)
        intermediateAdd[key] = value
    }

    override fun removeProperty(key: String) {
        // remove the key from properties to add
        intermediateAdd.remove(key)
        intermediateDelete.add(key)
    }

    fun isModified(): Boolean {
        return intermediateAdd.isNotEmpty() || intermediateDelete.isNotEmpty()
    }

    fun apply() {
        intermediateAdd.forEach { (key, value) ->
            delegate.setProperty(key, value)
        }
        intermediateAdd.clear()

        intermediateDelete.forEach {
            delegate.removeProperty(it)
        }
        intermediateDelete.clear()
    }

    fun reset() {
        intermediateAdd.clear()
        intermediateDelete.clear()
    }
}
