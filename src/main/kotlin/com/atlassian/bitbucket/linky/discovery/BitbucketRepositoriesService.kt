package com.atlassian.bitbucket.linky.discovery

import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.BitbucketRepository.Cloud
import com.atlassian.bitbucket.linky.BitbucketRepository.Server
import com.atlassian.bitbucket.linky.hosting.BitbucketCloudRegistry
import com.atlassian.bitbucket.linky.hosting.BitbucketServerRegistry
import com.atlassian.bitbucket.linky.preferences.preferences
import com.atlassian.bitbucket.linky.repository.alwaysLinkToSelectedRemote
import com.atlassian.bitbucket.linky.repository.getOrderedRemoteUrls
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project

interface BitbucketRepositoriesService {
    fun getBitbucketRepositories(repository: Repository): Map<RemoteUrl, BitbucketRepository>
    fun getBitbucketRepository(repository: Repository): BitbucketRepository?

    fun configurationChanged()
}

class DefaultBitbucketRepositoriesService(private val project: Project) : BitbucketRepositoriesService {

    override fun getBitbucketRepositories(repository: Repository): Map<RemoteUrl, BitbucketRepository> =
        repository.remoteUrlCandidates()
            .mapNotNull { remoteUrl -> getBitbucketRepository(repository, remoteUrl)?.let { remoteUrl to it } }
            .toMap()

    override fun getBitbucketRepository(repository: Repository): BitbucketRepository? =
        repository.remoteUrlCandidates()
            .mapNotNull { remoteUrl -> getBitbucketRepository(repository, remoteUrl) }
            .firstOrNull()

    override fun configurationChanged() {
        project.discoverRepositories()
    }

    private fun getBitbucketRepository(repository: Repository, remoteUrl: RemoteUrl): BitbucketRepository? {
        if (remoteUrl.matchesCloudPattern()) {
            val (workspace, slug) = remoteUrl.cloudWorkspaceAndSlug()
            val cloudRegistry = service<BitbucketCloudRegistry>()
            cloudRegistry.lookup(remoteUrl.scheme, remoteUrl.hostname, remoteUrl.port)
                ?.let { return Cloud(repository, remoteUrl, workspace, slug) }
        }
        if (remoteUrl.matchesServerPattern()) {
            val (appPath, project, slug) = remoteUrl.serverPathProjectAndSlug()
            val serverRegistry = service<BitbucketServerRegistry>()
            serverRegistry.lookup(remoteUrl.scheme, remoteUrl.hostname, remoteUrl.port, appPath)
                ?.let { return Server(repository, remoteUrl, it, project, slug) }
        }
        return null
    }

    private fun Repository.remoteUrlCandidates(): List<RemoteUrl> =
        // if specific remote URL is defined in properties, use it
        preferences().getProperty(alwaysLinkToSelectedRemote)
            // fallback to remote URL selected based on the repository
            ?.let { serializedRemote -> RemoteUrl.parse(serializedRemote) }
            ?.let { listOf(it) }
            // same for the case when no specific remote URL is defined
            ?: getOrderedRemoteUrls()
}
