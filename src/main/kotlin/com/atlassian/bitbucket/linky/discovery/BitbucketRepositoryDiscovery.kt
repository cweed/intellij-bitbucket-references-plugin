package com.atlassian.bitbucket.linky.discovery

import com.atlassian.bitbucket.linky.repository.traverseRepositories
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.project.Project
import com.intellij.util.messages.Topic

fun Project.discoverRepositories() {
    traverseRepositories().forEach { RepositoryAnalyzer.analyzeRepository(it) }
    ApplicationManager.getApplication()
        .messageBus
        .syncPublisher(BitbucketRepositoriesDiscoveryListener.topic)
        .repositoriesDiscoveryCompleted(this)
}

interface BitbucketRepositoriesDiscoveryListener {
    companion object {
        val topic = Topic.create(
            "BitbucketRepositories.Discovery",
            BitbucketRepositoriesDiscoveryListener::class.java
        )
    }

    fun repositoriesDiscoveryCompleted(project: Project)
}
