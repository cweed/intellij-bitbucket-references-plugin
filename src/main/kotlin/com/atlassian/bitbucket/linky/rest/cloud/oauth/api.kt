package com.atlassian.bitbucket.linky.rest.cloud.oauth

import com.atlassian.bitbucket.linky.rest.auth.OAuthConsumer
import com.atlassian.bitbucket.linky.rest.auth.OAuthTokens.RefreshAndAccessTokens
import com.atlassian.bitbucket.linky.rest.auth.RefreshToken
import java.util.concurrent.CompletableFuture

interface BitbucketCloudOAuthApi {
    fun requestOAuthTokens(authorizationCode: AuthorizationCode): CompletableFuture<RefreshAndAccessTokens>

    fun refreshOAuthTokens(refreshToken: RefreshToken): CompletableFuture<RefreshAndAccessTokens>
}

class BitbucketCloudOAuthApiConfig {
    lateinit var consumer: OAuthConsumer
}

@JvmInline
value class AuthorizationCode(val value: String)
