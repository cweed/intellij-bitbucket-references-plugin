package com.atlassian.bitbucket.linky.rest.cloud

import com.atlassian.bitbucket.linky.rest.PullRequest
import com.atlassian.bitbucket.linky.rest.mapException
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.gson.responseObject
import com.google.gson.Gson
import org.apache.http.client.utils.URIBuilder
import java.net.URI
import java.time.ZonedDateTime
import java.util.concurrent.CompletableFuture

class CommitApiImpl(
    private val fuel: FuelManager,
    private val gson: Gson,
    private val repositoryId: RepositoryId,
    private val commit: String
) : CommitApi {
    override fun relatedPullRequests(pageNumber: Int): CompletableFuture<CloudPage<PullRequest>> {
        val future = CompletableFuture<CloudPage<PullRequest>>()

        fuel.get(BitbucketCloud.relatedPullRequestsUrl(repositoryId, commit, pageNumber).toString())
            .responseObject<PagedBean<PullRequestBean>>(gson) { _, response, result ->
                result.fold(
                    success = {
                        when (response.statusCode) {
                            202 -> future.completeExceptionally(PullRequestLinksIndexingException(repositoryId))
                            else -> future.complete(it.toPullRequestsPage())
                        }
                    },
                    failure = {
                        future.completeExceptionally(
                            it.mapException("commit pull requests") { code ->
                                when (code) {
                                    400, 404 -> PullRequestLinksNotEnabledException(repositoryId)
                                    else -> null
                                }
                            }
                        )
                    }
                )
            }
        return future
    }

    private fun PagedBean<PullRequestBean>.toPullRequestsPage(): CloudPage<PullRequest> {
        fun pageFetcher(pageNumber: Int) = { api: BitbucketCloudApi ->
            api.repository(repositoryId)
                .commit(commit)
                .relatedPullRequests(pageNumber)
        }
        return CloudPage(
            items = values.map { it.toPullRequest() },
            pageSize = pagelen,
            pageNumber = page,
            nextPage = next?.let { pageFetcher(page + 1) }
        )
    }
}

private fun BitbucketCloud.relatedPullRequestsUrl(
    repositoryId: RepositoryId,
    commitHash: String,
    pageNumber: Int
): URI {
    val paginationFields = listOf(
        "page",
        "pagelen",
        "size",
        "previous",
        "next"
    )
    val pullRequestFields = listOf(
        "id",
        "title",
        "description",
        "source.branch.name",
        "destination.branch.name",
        "state",
        "author.display_name",
        "author.nickname",
        "created_on",
        "links.html.href",
        "author.type"
    ).map { "values.$it" }
    return URIBuilder(apiBaseUrl)
        .setPathSegments(
            "2.0",
            "repositories",
            repositoryId.workspace,
            repositoryId.slug,
            "commit",
            commitHash,
            "pullrequests",
        )
        .addParameter("fields", (paginationFields + pullRequestFields).joinToString(separator = ","))
        .addParameter("page", "$pageNumber")
        .build()
}

private data class PullRequestBean(
    val id: Long,
    val title: String,
    val description: String?,
    val source: CommitCoordinatesBean,
    val destination: CommitCoordinatesBean,
    val state: PullRequestState,
    val author: AccountBean?,
    val createdOn: ZonedDateTime,
    val links: HtmlLinksBean
) {
    fun toPullRequest() = PullRequest(
        id = id,
        title = title,
        description = description,
        sourceBranchName = source.branch.name,
        destinationBranchName = destination.branch.name,
        state = state.name,
        authorName = author?.let { it.displayName ?: it.nickname },
        createdDate = createdOn,
        link = links.uri()
    )
}

private data class CommitCoordinatesBean(val branch: BranchBean)

private data class BranchBean(val name: String)
