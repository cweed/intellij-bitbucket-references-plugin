package com.atlassian.bitbucket.linky.rest.server

import java.net.URI

data class SelfLinksBean(val self: List<LinkBean>) {
    fun uri(): URI = URI.create(self.first().href)
}

data class LinkBean(val href: String)
