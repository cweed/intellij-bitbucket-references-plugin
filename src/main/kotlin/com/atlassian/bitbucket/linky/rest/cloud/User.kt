package com.atlassian.bitbucket.linky.rest.cloud

import java.net.URI
import java.util.UUID

data class User(
    val uuid: UUID,
    val name: String,
    val avatarUrl: URI,
)
