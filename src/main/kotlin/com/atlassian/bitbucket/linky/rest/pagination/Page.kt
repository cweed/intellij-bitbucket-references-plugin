package com.atlassian.bitbucket.linky.rest.pagination

import java.util.concurrent.CompletableFuture

data class Page<A, T>(
    val items: List<T>,
    val pageSize: Int,
    val pageNumber: Int? = null,
    val totalSize: Int? = null,
    val nextPage: ((A) -> CompletableFuture<Page<A, T>>)?
)

fun <A, T> CompletableFuture<Page<A, T>>.fetchRemaining(
    api: A,
    consumeItems: (List<T>) -> Unit
): CompletableFuture<Page<A, T>?> {
    return thenCompose { page ->
        consumeItems(page.items)
        page.nextPage?.invoke(api)
            ?.fetchRemaining(api, consumeItems)
            ?: CompletableFuture.completedFuture<Page<A, T>?>(null)
    }
}
