package com.atlassian.bitbucket.linky.rest.cloud

import java.time.ZonedDateTime
import java.util.UUID

data class Pipeline(
    val uuid: UUID,
    val status: PipelineStatus,
    val creator: User?,
    val buildNumber: Long,
    val createdOn: ZonedDateTime,
    val completedOn: ZonedDateTime?,
    val target: PipelineTarget,
    val trigger: PipelineTrigger,
)

sealed class PipelineStatus {
    object Pending : PipelineStatus()
    object Halted : PipelineStatus()
    object Paused : PipelineStatus()
    object Running : PipelineStatus()
    data class Error(val key: String?, val message: String?) : PipelineStatus()
    data class Failed(val key: String?, val message: String?) : PipelineStatus()
    data class Stopped(val actor: User?) : PipelineStatus()
    object Successful : PipelineStatus()

    object Unknown : PipelineStatus()

    override fun toString(): String = this.javaClass.simpleName
}

enum class PipelineTrigger {
    PUSH, SCHEDULE, MANUAL,
}

sealed class PipelineTarget {
    data class BareCommit(val commit: Commit) : PipelineTarget()
    data class Branch(val commit: Commit, val branch: String) : PipelineTarget()
    data class Tag(val commit: Commit, val tag: String) : PipelineTarget()
    data class PullRequest(
        val commit: Commit,
        val prId: Long,
        val prTitle: String,
        val sourceBranch: String,
        val destinationBranch: String,
    ) : PipelineTarget()

    object Unknown : PipelineTarget()
}

data class Commit(val hash: String, val message: String)
