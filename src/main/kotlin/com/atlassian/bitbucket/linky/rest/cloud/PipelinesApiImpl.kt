package com.atlassian.bitbucket.linky.rest.cloud

import com.atlassian.bitbucket.linky.rest.mapException
import com.atlassian.bitbucket.linky.rest.pagination.Page
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.gson.responseObject
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import org.apache.http.client.utils.URIBuilder
import java.lang.reflect.Type
import java.time.ZonedDateTime
import java.util.UUID
import java.util.concurrent.CompletableFuture

private const val PAGE_SIZE = 10

class PipelinesApiImpl(
    private val fuel: FuelManager,
    private val gson: Gson,
    private val repositoryId: RepositoryId,
) : PipelinesApi {

    override fun page(pageNumber: Int): CompletableFuture<CloudPage<Pipeline>> {
        val future = CompletableFuture<CloudPage<Pipeline>>()
        fuel.get(
            URIBuilder(BitbucketCloud.apiBaseUrl)
                .setPathSegments("2.0", "repositories", repositoryId.workspace, repositoryId.slug, "pipelines")
                // Trailing slash is required for Bitbucket to accept this request
                .build().toString() + "/",
            listOf(
                "pagelen" to PAGE_SIZE,
                "page" to pageNumber,
                "fields" to "+values.target.commit.message",
                "sort" to "-created_on",
            )
        ).responseObject<PagedBean<PipelineBean>>(gson) { _, _, result ->
            result.fold(
                success = { future.complete(it.toPipelinesPage(PAGE_SIZE)) },
                failure = { future.completeExceptionally(it.mapException("pipelines")) }
            )
        }
        return future
    }

    private fun PagedBean<PipelineBean>.toPipelinesPage(requestedPageSize: Int): CloudPage<Pipeline> {
        // Pipelines API returns incorrect `pagelen` and no `next` or `previous` links
        // so we do some heuristics here to calculate them

        // Size is returned by this API endpoint
        val size = this.size ?: throw IllegalStateException("Pipelines API should return 'size' attribute")
        // There's next page only if this page is full, and we covered less items than total
        val hasNextPage = (requestedPageSize == values.size) && (page * requestedPageSize < size)

        fun pageFetcher(pageNumber: Int) = { api: BitbucketCloudApi ->
            api.repository(repositoryId).pipelines().page(pageNumber)
        }

        return Page(
            items = values.map { it.toPipeline() },
            pageSize = requestedPageSize,
            pageNumber = page,
            totalSize = size,
            nextPage = if (hasNextPage) pageFetcher(page + 1) else null
        )
    }
}

fun GsonBuilder.registerPipelinesAdapters(): GsonBuilder =
    registerTypeAdapter(
        TargetBean::class.java,
        object : JsonDeserializer<TargetBean> {
            override fun deserialize(json: JsonElement, type: Type, ctx: JsonDeserializationContext): TargetBean =
                when (json.asJsonObject.getAsJsonPrimitive("type").asString) {
                    "pipeline_ref_target" -> ctx.deserialize(json, RefTargetBean::class.java)
                    "pipeline_commit_target" -> ctx.deserialize(json, CommitTargetBean::class.java)
                    "pipeline_pullrequest_target" -> ctx.deserialize(json, PullRequestTargetBean::class.java)
                    else -> object : TargetBean {
                        override fun toTarget(): PipelineTarget = PipelineTarget.Unknown
                    }
                }
        }
    )

private class PipelineBean(
    val uuid: String,
    val state: StateBean,
    val creator: UserBean?,
    val buildNumber: Long,
    val createdOn: ZonedDateTime,
    val completedOn: ZonedDateTime?,
    val target: TargetBean,
    val trigger: TriggerBean,
) {
    fun toPipeline() =
        Pipeline(
            parseBitbucketUuid(uuid),
            state.toStatus(),
            creator?.toUser(),
            buildNumber,
            createdOn,
            completedOn,
            target.toTarget(),
            trigger.toTrigger(),
        )
}

private class StateBean(
    val name: String,
    val stage: StageBean?,
    val result: ResultBean?,
) {
    fun toStatus(): PipelineStatus =
        when (name) {
            "PENDING" -> PipelineStatus.Pending
            "IN_PROGRESS" -> when (stage?.name ?: "") {
                "RUNNING" -> PipelineStatus.Running
                "PAUSED" -> PipelineStatus.Paused
                "HALTED" -> PipelineStatus.Halted
                else -> PipelineStatus.Unknown
            }
            "COMPLETED" -> when (result?.name ?: "") {
                "ERROR" -> PipelineStatus.Error(result?.error?.key, result?.error?.message)
                "FAILED" -> PipelineStatus.Failed(result?.error?.key, result?.error?.message)
                "STOPPED" -> PipelineStatus.Stopped(result?.terminator?.toUser())
                "SUCCESSFUL" -> PipelineStatus.Successful
                else -> PipelineStatus.Unknown
            }
            else -> PipelineStatus.Unknown
        }
}

private class StageBean(
    val name: String,
)

private class ResultBean(
    val name: String,
    val error: ErrorBean?,
    val terminator: UserBean?,
)

private class ErrorBean(
    val key: String?,
    val message: String?,
)

private class UserBean(
    val uuid: String,
    val displayName: String,
    val links: AvatarLinksBean,
) {
    fun toUser(): User = User(parseBitbucketUuid(uuid), displayName, links.uri())
}

interface TargetBean {
    fun toTarget(): PipelineTarget
}

private class CommitTargetBean(
    val commit: CommitBean,
) : TargetBean {
    override fun toTarget(): PipelineTarget = PipelineTarget.BareCommit(commit.toCommit())
}

private class RefTargetBean(
    val commit: CommitBean,
    val refType: String,
    val refName: String,
) : TargetBean {
    override fun toTarget(): PipelineTarget =
        when (refType) {
            "branch" -> PipelineTarget.Branch(commit.toCommit(), refName)
            "tag" -> PipelineTarget.Tag(commit.toCommit(), refName)
            else -> PipelineTarget.Unknown
        }
}

private class PullRequestTargetBean(
    val commit: CommitBean,
    val pullrequest: PullRequestInfoBean,
    val source: String,
    val destination: String,
) : TargetBean {
    override fun toTarget(): PipelineTarget =
        PipelineTarget.PullRequest(commit.toCommit(), pullrequest.id, pullrequest.title, source, destination)
}

private class CommitBean(
    val hash: String,
    val message: String?,
) {
    fun toCommit() = Commit(hash, message ?: "")
}

private class PullRequestInfoBean(
    val id: Long,
    val title: String,
)

private class TriggerBean(
    val name: String,
) {
    fun toTrigger() = PipelineTrigger.valueOf(name)
}

private fun parseBitbucketUuid(uuid: String): UUID = UUID.fromString(uuid.trim('{', '}'))
