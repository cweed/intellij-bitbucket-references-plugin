package com.atlassian.bitbucket.linky.rest.server

data class PagedBean<T>(
    val start: Int,
    val isLastPage: Boolean,
    val limit: Int,
    val size: Int,
    val nextPageStart: Int? = null,
    val values: List<T> = emptyList()
)
