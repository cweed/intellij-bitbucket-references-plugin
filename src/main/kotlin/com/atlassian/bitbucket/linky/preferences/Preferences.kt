package com.atlassian.bitbucket.linky.preferences

interface Preferences {
    fun getProperty(key: String, defaultValue: String? = null): String?
    fun setProperty(key: String, value: String)
    fun removeProperty(key: String)
}
