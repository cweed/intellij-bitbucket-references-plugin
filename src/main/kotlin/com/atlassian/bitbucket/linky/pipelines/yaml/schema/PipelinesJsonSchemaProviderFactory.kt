package com.atlassian.bitbucket.linky.pipelines.yaml.schema

import com.atlassian.bitbucket.linky.pipelines.yaml.BitbucketPipelinesYamlFileType
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VfsUtil
import com.intellij.openapi.vfs.VirtualFile
import com.jetbrains.jsonSchema.extension.JsonSchemaFileProvider
import com.jetbrains.jsonSchema.extension.JsonSchemaProviderFactory
import com.jetbrains.jsonSchema.extension.SchemaType

object PipelinesJsonSchemaProviderFactory : JsonSchemaProviderFactory {
    override fun getProviders(project: Project) = listOf(PipelinesJsonSchemaProvider)
}

object PipelinesJsonSchemaProvider : JsonSchemaFileProvider {

    override fun getName() = "Bitbucket Pipelines configuration"

    override fun isAvailable(file: VirtualFile) =
        file.fileType == BitbucketPipelinesYamlFileType

    @Suppress("CanBeVal")
    override fun getSchemaFile(): VirtualFile? {
        val path = if (service<PipelinesPreferences>().useInternalJsonSchema()) {
            "/schemas/bitbucket-pipelines-internal.schema.json"
        } else {
            "/schemas/bitbucket-pipelines.schema.json"
        }
        val resource = javaClass.getResource(path) ?: throw IllegalStateException("No schema found at $path")
        return VfsUtil.findFileByURL(resource)
    }

    override fun getSchemaType() = SchemaType.embeddedSchema
}
