package com.atlassian.bitbucket.linky.pipelines.yaml.inspection

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.cacheDefinitionPattern
import com.intellij.codeInspection.LocalInspectionTool
import com.intellij.codeInspection.ProblemHighlightType.GENERIC_ERROR_OR_WARNING
import com.intellij.codeInspection.ProblemsHolder
import com.intellij.psi.PsiElementVisitor
import org.jetbrains.yaml.psi.YAMLKeyValue
import org.jetbrains.yaml.psi.YamlPsiElementVisitor
import java.util.regex.Pattern

private val CACHE_NAME_PATTERN = Pattern.compile("[a-z0-9]([-a-z0-9]*[a-z0-9])?")
private const val MAX_CACHE_NAME_LENGTH = 50

class InvalidCacheDefinitionName : LocalInspectionTool() {

    override fun buildVisitor(
        holder: ProblemsHolder,
        isOnTheFly: Boolean
    ): PsiElementVisitor = object : YamlPsiElementVisitor() {
        override fun visitKeyValue(keyValue: YAMLKeyValue) {
            if (cacheDefinitionPattern.accepts(keyValue)) {
                val cacheName = keyValue.keyText

                fun problem(message: String) =
                    keyValue.key?.let { keyElement ->
                        holder.registerProblem(keyElement, message, GENERIC_ERROR_OR_WARNING)
                    }

                when {
                    cacheName.length > MAX_CACHE_NAME_LENGTH ->
                        problem(message("inspections.invalid.cache.name.too.long.problem.text"))
                    CACHE_NAME_PATTERN.matcher(cacheName).matches().not() ->
                        problem(message("inspections.invalid.cache.name.problem.text"))
                }
            }
        }
    }
}
