package com.atlassian.bitbucket.linky.pipelines.ui

import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowFactory
import com.intellij.ui.content.ContentFactory
import javax.swing.JLabel

class PipelinesToolWindowFactory : ToolWindowFactory {
    companion object {
        const val PIPELINES_TOOL_WINDOW_ID = "BitbucketPipelines"
    }

    override fun createToolWindowContent(project: Project, toolWindow: ToolWindow) {
        toolWindow.contentManager.addContent(
            ContentFactory.SERVICE.getInstance()
                .createContent(JLabel("Pipelines"), "Pipelines", true)
                .apply {
                    isCloseable = false
                }
        )
    }
}
