package com.atlassian.bitbucket.linky.pipelines.yaml.inspection

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.inPipelinesFilePattern
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.yamlKeyValuePattern
import com.atlassian.bitbucket.linky.pipelines.yaml.schema.PipelinesPreferences
import com.intellij.codeInspection.LocalInspectionTool
import com.intellij.codeInspection.LocalQuickFix
import com.intellij.codeInspection.ProblemDescriptor
import com.intellij.codeInspection.ProblemHighlightType.LIKE_UNKNOWN_SYMBOL
import com.intellij.codeInspection.ProblemsHolder
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.patterns.ElementPattern
import com.intellij.patterns.PlatformPatterns.psiElement
import com.intellij.psi.PsiElementVisitor
import org.jetbrains.yaml.psi.YAMLKeyValue
import org.jetbrains.yaml.psi.YAMLValue
import org.jetbrains.yaml.psi.YamlPsiElementVisitor

class InternalStepSizeInspection : LocalInspectionTool() {
    private val stepSizePattern: ElementPattern<YAMLKeyValue> =
        psiElement(YAMLKeyValue::class.java)
            .and(inPipelinesFilePattern)
            .and(yamlKeyValuePattern("size"))
            .withSuperParent(2, yamlKeyValuePattern("step"))

    override fun buildVisitor(
        holder: ProblemsHolder,
        isOnTheFly: Boolean,
    ): PsiElementVisitor = object : YamlPsiElementVisitor() {
        override fun visitKeyValue(keyValue: YAMLKeyValue) {
            if (service<PipelinesPreferences>().useInternalJsonSchema()) return

            if (stepSizePattern.accepts(keyValue)) {
                if (keyValue.valueText == "4x") {
                    keyValue.value?.let { valueElement ->
                        holder.registerProblem(
                            valueElement,
                            message("inspections.internal.step.size.problem.text"),
                            LIKE_UNKNOWN_SYMBOL,
                            UseInternalPipelinesJsonSchemaQuickFix(
                                message("inspections.internal.step.size.problem.fix.family.name"),
                                message("inspections.internal.step.size.problem.fix.name"),
                            )
                        )
                    }
                }
            }
        }
    }
}

private class UseInternalPipelinesJsonSchemaQuickFix(
    private val familyName: String,
    private val name: String,
) : LocalQuickFix {
    override fun getFamilyName(): String = familyName
    override fun getName(): String = name

    override fun applyFix(project: Project, descriptor: ProblemDescriptor) {
        service<PipelinesPreferences>().doUseInternalJsonSchema()

        val value = descriptor.psiElement as? YAMLValue ?: return
        // Replace value with itself to trigger document updated event
        // which, in turn, triggers re-validation against JSON schema
        value.replace(value)
    }
}
