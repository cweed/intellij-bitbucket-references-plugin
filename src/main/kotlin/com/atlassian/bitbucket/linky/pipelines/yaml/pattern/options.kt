package com.atlassian.bitbucket.linky.pipelines.yaml.pattern

import com.intellij.patterns.ElementPattern
import com.intellij.patterns.PlatformPatterns
import org.jetbrains.yaml.psi.YAMLDocument
import org.jetbrains.yaml.psi.YAMLKeyValue

val dockerOptionPattern: ElementPattern<YAMLKeyValue> =
    PlatformPatterns.psiElement(YAMLKeyValue::class.java)
        .and(inPipelinesFilePattern)
        .and(yamlKeyValuePattern("docker"))
        .and(withinYamlObject("options"))
        .withSuperParent(4, YAMLDocument::class.java)
