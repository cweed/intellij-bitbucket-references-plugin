package com.atlassian.bitbucket.linky.pipelines.yaml.pattern

import com.atlassian.bitbucket.linky.pipelines.yaml.BitbucketPipelinesYamlFileType
import com.intellij.codeInsight.completion.CompletionUtilCore
import com.intellij.patterns.ElementPattern
import com.intellij.patterns.PatternCondition
import com.intellij.patterns.PlatformPatterns.psiElement
import com.intellij.patterns.PlatformPatterns.virtualFile
import com.intellij.psi.PsiElement
import com.intellij.util.ProcessingContext
import org.jetbrains.yaml.psi.YAMLKeyValue
import org.jetbrains.yaml.psi.YAMLMapping

val inPipelinesFilePattern: ElementPattern<PsiElement> =
    psiElement()
        .inVirtualFile(
            virtualFile()
                .ofType(BitbucketPipelinesYamlFileType)
        )

val pipelinesListObjectPattern: ElementPattern<PsiElement> =
    psiElement()
        .and(inPipelinesFilePattern)
        .andOr(
            withinYamlObject("bookmarks"),
            withinYamlObject("branches"),
            withinYamlObject("custom"),
            withinYamlObject("pull-requests"),
            withinYamlObject("tags")
        )

fun withinYamlObject(name: String): ElementPattern<PsiElement> =
    psiElement()
        .andOr(
            psiElement()
                .withSuperParent(2, YAMLMapping::class.java)
                .withSuperParent(3, yamlKeyValuePattern(name)),
            psiElement()
                .withSuperParent(2, yamlKeyValuePattern(name))
        )

fun yamlKeyValuePattern(name: String): ElementPattern<YAMLKeyValue> =
    psiElement(YAMLKeyValue::class.java)
        .with(object : PatternCondition<YAMLKeyValue>("yaml_key_value:$name") {
            override fun accepts(t: YAMLKeyValue, context: ProcessingContext?): Boolean {
                return t.keyText.removeSuffix(CompletionUtilCore.DUMMY_IDENTIFIER_TRIMMED) == name
            }
        })
