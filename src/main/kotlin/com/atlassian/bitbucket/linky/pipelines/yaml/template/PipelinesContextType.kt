package com.atlassian.bitbucket.linky.pipelines.yaml.template

import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.inPipelinesFilePattern
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.pipelinesListObjectPattern
import com.intellij.codeInsight.template.TemplateContextType
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.util.PsiUtilCore
import org.jetbrains.yaml.YAMLLanguage

class PipelinesContextType : TemplateContextType(
    "BITBUCKET_PIPELINES",
    "Bitbucket Pipelines"
) {
    override fun isInContext(file: PsiFile, offset: Int) =
        pipelinesElement(file, offset) != null
}

class PipelinesListObjectContextType : TemplateContextType(
    "PIPELINES_LIST_OBJECT",
    "Pipelines list object",
    PipelinesContextType::class.java
) {
    override fun isInContext(file: PsiFile, offset: Int) =
        pipelinesElement(file, offset)
            ?.let { pipelinesListObjectPattern.accepts(it) }
            ?: false
}

private fun pipelinesElement(file: PsiFile, offset: Int): PsiElement? {
    if (inPipelinesFilePattern.accepts(file) && !isEmbeddedContent(file, offset)) {
        return file.findElementAt(offset)
    }
    return null
}

private fun isEmbeddedContent(file: PsiFile, offset: Int): Boolean {
    val languageAtOffset = PsiUtilCore.getLanguageAtOffset(file, offset)
    return !languageAtOffset.isKindOf(YAMLLanguage.INSTANCE)
}
