package com.atlassian.bitbucket.linky.pipelines.yaml.resolve

import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.cacheDefinitionPattern
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.dockerOptionPattern
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.serviceDefinitionPattern
import com.intellij.psi.PsiFile
import com.intellij.psi.util.CachedValueProvider
import com.intellij.psi.util.CachedValuesManager
import com.intellij.util.containers.MultiMap
import org.jetbrains.yaml.psi.YAMLKeyValue
import org.jetbrains.yaml.psi.YamlRecursivePsiElementVisitor

fun resolveCacheDefinitions(file: PsiFile): MultiMap<String, YAMLKeyValue> =
    resolveDefinitions(file).cacheDefinitions

fun resolveServiceDefinitions(file: PsiFile): MultiMap<String, YAMLKeyValue> =
    resolveDefinitions(file).serviceDefinitions

data class Options(val dockerEnabled: Boolean)

fun resolveOptions(file: PsiFile): Options =
    CachedValuesManager.getCachedValue(file) {
        var dockerEnabled = false
        file.accept(object : YamlRecursivePsiElementVisitor() {
            override fun visitKeyValue(keyValue: YAMLKeyValue) {
                if (dockerOptionPattern.accepts(keyValue)) {
                    dockerEnabled = keyValue.valueText.toBoolean()
                } else {
                    super.visitKeyValue(keyValue)
                }
            }
        })

        CachedValueProvider.Result.create(Options(dockerEnabled), file)
    }

private fun resolveDefinitions(file: PsiFile): DefinitionsResolveResult =
    CachedValuesManager.getCachedValue(file) {
        val cacheDefinitions = MultiMap<String, YAMLKeyValue>()
        val serviceDefinitions = MultiMap<String, YAMLKeyValue>()

        file.accept(object : YamlRecursivePsiElementVisitor() {
            override fun visitKeyValue(keyValue: YAMLKeyValue) {
                when {
                    cacheDefinitionPattern.accepts(keyValue) ->
                        cacheDefinitions.putValue(keyValue.keyText, keyValue)
                    serviceDefinitionPattern.accepts(keyValue) ->
                        serviceDefinitions.putValue(keyValue.keyText, keyValue)
                    else -> super.visitKeyValue(keyValue)
                }
            }
        })

        val result = DefinitionsResolveResult(cacheDefinitions, serviceDefinitions)
        CachedValueProvider.Result.create(result, file)
    }

private class DefinitionsResolveResult(
    val cacheDefinitions: MultiMap<String, YAMLKeyValue>,
    val serviceDefinitions: MultiMap<String, YAMLKeyValue>
)
