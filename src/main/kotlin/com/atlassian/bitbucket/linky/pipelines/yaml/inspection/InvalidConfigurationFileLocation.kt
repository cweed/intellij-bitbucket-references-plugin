package com.atlassian.bitbucket.linky.pipelines.yaml.inspection

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.pipelines.yaml.BitbucketPipelinesYamlFileType
import com.atlassian.bitbucket.linky.pipelines.yaml.vcs.repositoryForFile
import com.intellij.codeInspection.InspectionManager
import com.intellij.codeInspection.LocalInspectionTool
import com.intellij.codeInspection.LocalQuickFix
import com.intellij.codeInspection.ProblemDescriptor
import com.intellij.codeInspection.ProblemHighlightType.WARNING
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VfsUtilCore
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiManager
import com.intellij.refactoring.move.moveFilesOrDirectories.MoveFilesOrDirectoriesUtil

class InvalidConfigurationFileLocation : LocalInspectionTool() {

    override fun checkFile(
        file: PsiFile,
        manager: InspectionManager,
        isOnTheFly: Boolean
    ): Array<ProblemDescriptor>? {
        val virtualFile = file.virtualFile
        if (virtualFile.fileType == BitbucketPipelinesYamlFileType) {
            val repository = repositoryForFile(file) ?: return null
            val relativePath = VfsUtilCore.getRelativePath(virtualFile, repository.root) ?: return null

            if (relativePath.contains('/')) {
                return arrayOf(
                    manager.createProblemDescriptor(
                        file,
                        message("inspections.file.location.problem.text"),
                        MoveFileToRepositoryRootQuickFix(virtualFile.name),
                        WARNING,
                        isOnTheFly
                    )
                )
            }
        }
        return null
    }
}

class MoveFileToRepositoryRootQuickFix(private val filename: String) : LocalQuickFix {

    override fun getName(): String {
        return message("inspections.file.location.problem.fix.name", filename)
    }

    override fun getFamilyName(): String = message("inspections.file.location.problem.fix.family.name")

    override fun applyFix(project: Project, descriptor: ProblemDescriptor) {
        val file = descriptor.psiElement as? PsiFile ?: return
        val repository = repositoryForFile(file) ?: return
        val repositoryRoot = PsiManager.getInstance(project).findDirectory(repository.root) ?: return

        MoveFilesOrDirectoriesUtil.doMoveFile(file, repositoryRoot)
    }
}
