package com.atlassian.bitbucket.linky

import com.intellij.openapi.diagnostic.Logger

fun logger(): Logger = Logger.getInstance(LOGGER_NAME)
