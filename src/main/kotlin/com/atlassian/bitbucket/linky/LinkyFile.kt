package com.atlassian.bitbucket.linky

import com.atlassian.bitbucket.linky.blame.LineBlamer
import com.atlassian.bitbucket.linky.revision.VcsRevisionResolver
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.components.service
import com.intellij.openapi.vcs.history.VcsRevisionNumber
import com.intellij.openapi.vfs.VirtualFile

typealias RelativePath = String

class LinkyFile(
    val virtualFile: VirtualFile,
    val repository: Repository,
    val revision: Revision,
    val relativePath: RelativePath,
    private val lineBlamer: LineBlamer
) {
    val name = virtualFile.name

    fun blameLine(lineNumber: Int) = lineBlamer.blameLine(this, lineNumber)
}

val VcsRevisionNumber.revisionHash: Revision?
    get() = service<VcsRevisionResolver>().resolveVcsRevision(this)
