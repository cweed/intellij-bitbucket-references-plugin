package com.atlassian.bitbucket.linky.configuration;

import com.atlassian.bitbucket.linky.BitbucketRepository;
import com.atlassian.bitbucket.linky.discovery.BitbucketRepositoriesService;
import com.atlassian.bitbucket.linky.discovery.RemoteUrl;
import com.atlassian.bitbucket.linky.preferences.RepositoryPreferencesKt;
import com.atlassian.bitbucket.linky.repository.RepositoryExtensionsKt;
import com.intellij.dvcs.repo.Repository;
import com.intellij.ide.BrowserUtil;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.ComboBox;
import com.intellij.ui.CollectionComboBoxModel;
import com.intellij.ui.DocumentAdapter;
import com.intellij.ui.ListCellRendererWrapper;
import com.intellij.ui.components.JBTextField;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.atlassian.bitbucket.linky.repository.PreferencesKt.alwaysLinkToSelectedRemote;
import static com.atlassian.bitbucket.linky.repository.PreferencesKt.pullRequestDefaultTargetBranchName;

public class LinkedRepositoryConfigurationForm implements Configurable {
    private final Project project;
    private final TwoStepPreferences preferences;
    private final Repository repository;

    private JBTextField branchNameTextField;
    private JPanel mainPanel;
    private JLabel urlLabel;
    private JRadioButton automaticRadioButton;
    private JRadioButton fixedRadioButton;
    private ComboBox bbReposComboBox;

    private BitbucketRepository currentBitbucketRepository;

    private AtomicBoolean updatingState = new AtomicBoolean(false);

    LinkedRepositoryConfigurationForm(Project project, Repository repository) {
        this.project = project;
        this.preferences = new TwoStepPreferences(RepositoryPreferencesKt.preferences(repository));
        this.repository = repository;

        List<BitbucketRepository> bbRepos =
                new ArrayList<>(getBitbucketRepositoriesService().getBitbucketRepositories(repository).values());
        bbReposComboBox.setModel(new CollectionComboBoxModel<>(bbRepos));

        automaticRadioButton.addActionListener(event -> remoteSelectionChanged());
        fixedRadioButton.addActionListener(event -> remoteSelectionChanged());
    }

    @Override
    public void disposeUIResources() {
    }

    @NotNull
    @Override
    public JComponent createComponent() {
        return mainPanel;
    }

    @Nls
    @Override
    public String getDisplayName() {
        return "Bitbucket Repository Configuration";
    }

    @Nullable
    @Override
    public String getHelpTopic() {
        return null;
    }

    @Override
    public boolean isModified() {
        return preferences.isModified();
    }

    @Override
    public void apply() throws ConfigurationException {
        preferences.apply();
    }

    @Override
    public void reset() {
        updatingState.set(true);

        preferences.reset();
        branchNameTextField.setText(preferences.getProperty(pullRequestDefaultTargetBranchName, ""));

        Optional<RemoteUrl> fixedRemoteUrl = getFixedRemoteUrl();
        if (fixedRemoteUrl.isPresent()) {
            BitbucketRepository bbRepo =
                    getBitbucketRepositoriesService().getBitbucketRepositories(repository).get(fixedRemoteUrl.get());
            bbReposComboBox.setSelectedItem(bbRepo);
            fixedBitbucketRepositoryEnabled(bbRepo);
        } else {
            automaticSelectionEnabled();
        }

        updatingState.set(false);
    }

    private Optional<RemoteUrl> getFixedRemoteUrl() {
        return Optional.ofNullable(preferences.getProperty(alwaysLinkToSelectedRemote, null))
                .flatMap(fixedRemote -> Optional.ofNullable(RemoteUrl.Companion.parse(fixedRemote)));
    }

    private BitbucketRepositoriesService getBitbucketRepositoriesService() {
        return project.getService(BitbucketRepositoriesService.class);
    }

    private void createUIComponents() {
        urlLabel = new JLabel();
        urlLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
        urlLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                BrowserUtil.browse(currentBitbucketRepository.getBaseUri());
            }
        });

        bbReposComboBox = new ComboBox();
        bbReposComboBox.setRenderer(new ListCellRendererWrapper<BitbucketRepository>() {
            @Override
            public void customize(JList list, BitbucketRepository bbRepo, int index, boolean selected, boolean hasFocus) {
                setText(String.format("<html><b>%s:</b> %s</html>", bbRepo.getClass().getSimpleName(), bbRepo.getFullSlug()));
            }
        });
        bbReposComboBox.addActionListener(e -> remoteSelectionChanged());

        branchNameTextField = new JBTextField();
        branchNameTextField.getEmptyText().setText("<default branch>");
        branchNameTextField.getDocument().addDocumentListener(new DocumentAdapter() {
            @Override
            protected void textChanged(@NotNull DocumentEvent documentEvent) {
                String userInput = branchNameTextField.getText();
                String oldBranchName = preferences.getProperty(pullRequestDefaultTargetBranchName, "");
                if (!StringUtils.equals(userInput, oldBranchName)) {
                    if (StringUtils.isBlank(userInput)) {
                        preferences.removeProperty(pullRequestDefaultTargetBranchName);
                    } else {
                        preferences.setProperty(pullRequestDefaultTargetBranchName, userInput);
                    }
                }
            }
        });
    }

    private void remoteSelectionChanged() {
        if (updatingState.get()) {
            return;
        }

        if (automaticRadioButton.isSelected()) {
            preferences.removeProperty(alwaysLinkToSelectedRemote);
            automaticSelectionEnabled();
        } else {
            BitbucketRepository bbRepo = (BitbucketRepository) bbReposComboBox.getSelectedItem();
            if (bbRepo != null) {
                preferences.setProperty(alwaysLinkToSelectedRemote, bbRepo.getRemoteUrl().serialize());
                fixedBitbucketRepositoryEnabled(bbRepo);
            }
        }
    }

    private void automaticSelectionEnabled() {
        Map<RemoteUrl, BitbucketRepository> bbRepos = getBitbucketRepositoriesService().getBitbucketRepositories(repository);
        RepositoryExtensionsKt.getOrderedRemoteUrls(repository).stream()
                .map(bbRepos::get)
                .filter(Objects::nonNull)
                .findFirst()
                .ifPresent(bbRepo -> currentBitbucketRepository = bbRepo);
        updateBitbucketUrl();

        automaticRadioButton.setSelected(true);
        fixedRadioButton.setSelected(false);
        bbReposComboBox.setEnabled(false);
    }

    private void fixedBitbucketRepositoryEnabled(BitbucketRepository bbRepo) {
        currentBitbucketRepository = bbRepo;
        updateBitbucketUrl();

        automaticRadioButton.setSelected(false);
        fixedRadioButton.setSelected(true);
        bbReposComboBox.setEnabled(true);
    }

    private void updateBitbucketUrl() {
        urlLabel.setText("<html><a href=\"#\">" + currentBitbucketRepository.getBaseUri().toString() + "</a></html>");
    }
}
